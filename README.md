
# SkySpy


### What is SkySpy?

**SkySpy** is my latest project that I made for practice (and for fun), in which I want to make weather bots for different platforms like Telegram etc. the project is still in process but for now **SkySpySms** is sort of usable in that you can: 

* Send a 5 digit zip code to a number and you get back its' forecast  
* Send a key along with the zip code to get the forecast for a select time frame 
* Get help and a key list from the system

----
### So how does SkySpySms work?
First the SMS is sent to a messaging API, I used [Twilio](https://www.twilio.com)

[![Twilio](redmeImages/twilio-logo-red.png)](https://www.twilio.com)

because it was the easiest to set up since they have excellent support. (I used the webhooks approach in which the SMS is received by Twilio then they send a request to a URl and they send back the response in a SMS format.)


Since I am using Java I used [SparkJava](http://sparkjava.com/) for the server

[![SparkJava](redmeImages/sparkJava.png)](http://sparkjava.com/) 

since it has no configuration at all.....

Then for the zip code validation, I wanted to use a json object instead of a full-fledged database (to keep it simple) but couldn't find a good json file. So I found a text file with all the attributes I need and made a method that converts it to a json file (then I made a json object from it).

Then for the actual forecast API I used [Darksky API](https://darksky.net/dev/)

[![Darksky](redmeImages/poweredby-darksky.png)](https://darksky.net/poweredby/)

so if the zip code is valid I take the latitude and longitude from the zip code json object and send it to Darksky API (along with my API key) and I get back a json with the forecast.

Next I start to create the message. I take from the zip code json object the city and state and append it to the message, I take the timezone from the json that I got from the Darksky API and parse it for the location of the zip code and append it to the message.

Finally I parse the forecast (using org.json) for the time frame the user had requested, and append it to the message. Then I send it back to Twilio and Twilio sends it back as a SMS!!

 ----
### Language
For the language I used my primary programing language [Java](http://java.oracle.com/technetwork/java).
I made use of the latest release JDK 11 methods, for example this line is clear 

```java
return new JSONObject(Files.readString(Paths
                    .get(Paths.get("")
                        .toAbsolutePath().toString(), 
                            "Resources/zipcode.json")));               
```

but this is how I would have had to write it before JDK 11

```java
return new JSONObject(Files.readAllLines(Paths
                    .get(Paths.get("")
                        .toAbsolutePath().toString(), 
                            "Resources/zipcode.json"))
                    .get(0));

```
which is senseless since I never expect it to be more than one line, and I used the new HttpClient class as described in the [Java magazine](http://www.oracle.com/javamagazine) (November/December 2018 page 39).

----
### So what is SkySpy's number ?
Since I'm using Twilio it costs me $$ for each message, so regretfully I'm not able to share it here but you can request it for testing.

---
## So how can I try it?
First you can clone it and try it yourself but please get yourself a [Darksky API](https://darksky.net/dev/) key, its free and you don't even have to give them you're CC.  (Please don't use the key from here because the trial account allows up to 1,000 free calls per day.)

Or you can go to http://skyspy.tk/skyspy/sms/test?body=41567 and replace the zip code for your zip code and you will see on the page the response that you would get from Twilio. You can also get the list of all the keywords by going to http://skyspy.tk/skyspy/sms/test?body=key
and than you can add it to get the forecast for this time frame like this:  
" http://skyspy.tk/skyspy/sms/test?body=39648 weekly "

----
#
#
## So thanks for giving a look. Good luck!!


----
