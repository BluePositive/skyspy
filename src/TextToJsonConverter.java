import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class TextToJsonConverter {

	public static void createJsonFile() {
		File zipJson = new File("Resources/zipcode.json");
		try {
			zipJson.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<String> getTextFile() {
		try {
			return Files.readAllLines(
					Paths.get(Paths.get("").toAbsolutePath().toString(), "Resources/zipcode.text"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static DataOutputStream openOutputStream(String name) {
		DataOutputStream out = null;
		try {
			File file = new File(name);
			return new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		return out;
	}

	public static void writeJson() {
		createJsonFile();

		List<String> zipText = getTextFile();

		StringBuilder json = new StringBuilder();

		try {
			json.append("{");

			for (var i : zipText) {
				var c = i.split(",");
				json.append(c[0] + ":{\"city\":" + c[1] + ",\"state\":" + c[2] + ",\"latitude\":"
						+ c[3].substring(1, c[3].length() - 1) + ",\"longitude\":"
						+ c[4].substring(1, c[4].length() - 1) + "},");
			}

			json.append("}");

			DataOutputStream out = openOutputStream("Resources/zipcode.json");
			int last = json.lastIndexOf(",");
			out.writeBytes(json.substring(0, last).concat(json.substring(last + 1)));
			out.flush();
			out.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		writeJson();
		System.out.println("Done!");
	}
}
