package tk.skyspy.model;

public class ZipCode {

	private String zip;
	private String city;
	private String state;
	private Double latitude;
	private Double longitude;

	public ZipCode() {
		this.zip = "";
		this.city = "";
		this.state = "";
		this.latitude = 0.0;
		this.longitude = 0.0;
	}

	public ZipCode(String zip, String city, String state, Double latitude, Double longitude) {
		this.zip = zip;
		this.city = city;
		this.state = state;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "ZipCode [zip=" + zip + ", city=" + city + ", state=" + state + ", latitude="
				+ latitude + ", longitude=" + longitude + "]";
	}

}
