package tk.skyspy;

public class Pages {

	public static String get404() {

		return "<!DOCTYPE html><html lang=\"en\"><head><style type=\"text/css\">body {text-align: center;background-color:"
				+ " #00ffff;color: #000000;}header {color: #d7006b;font-family: monospace;  font-style: italic;font-size: "
				+ "2000%;text-shadow: 6px 6px 2px #ffcce6;}footer {font-size: 200%;}</style><title>404</title></head><body>"
				+ "<header>404</header><footer>The page you requested was not found. Are you sure you're in the right place?"
				+ "</footer></body></html>";
	}

	public static String getSkySpy() {

		return "<!DOCTYPE html><html lang=\"en\"><head><style type=\"text/css\">body {text-align: center;background-color: "
				+ "#000000;color: #ffffff;}header{color: #80ffff;font-family: monospace;font-style: italic;font-size: 1800%;"
				+ "text-shadow: 6px 6px 2px #ffcce6;}footer {font-size: 150%;font-family: inherit;}</style><title>SkySpy"
				+ "</title></head><body><br><br><br><header>SkySpy</header><footer><br><br><br>This site is now under "
				+ "construction.</footer></body></html>";
	}
}
