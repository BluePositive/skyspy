package tk.skyspy;

import static spark.Spark.get;
import static spark.Spark.internalServerError;
import static spark.Spark.notFound;
import static spark.Spark.port;
import static spark.Spark.post;

import com.twilio.twiml.MessagingResponse;
import com.twilio.twiml.messaging.Body;
import com.twilio.twiml.messaging.Message;

import tk.skyspy.sms.SmsProcessor;
import tk.skyspy.sms.Test;

public class SparkLauncher {

	public static void main(String[] args) {

	
		// This is the method that Twilio API will call
		post("skyspy/sms", (req, res) -> {
			try {
				res.type("application/xml");
				Body body = new Body.Builder(SmsProcessor.process(req.queryMap("Body").value()))
						.build();
				Message sms = new Message.Builder().body(body).build();
				MessagingResponse twiml = new MessagingResponse.Builder().message(sms).build();
				return twiml.toXml();
			} catch (Exception e) {
				return "Sorry but somthing went wrong\n" + e;
			}
		});

		get("skyspy/sms", (req, res) -> {

			try {
				res.type("application/xml");
				Body body = new Body.Builder(SmsProcessor.process(req.queryMap("Body").value()))
						.build();
				Message sms = new Message.Builder().body(body).build();
				MessagingResponse twiml = new MessagingResponse.Builder().message(sms).build();
				return twiml.toXml();
			} catch (Exception e) {
				return "Sorry but somthing went wrong\n" + e;
			}

		});

		// Just for testing
		get("skyspy/sms/test", (req, res) -> {

			try {
				return Test.process(req.queryMap("body").value()).replaceAll("\n", "<br>");
			} catch (Exception e) {
				return "Sorry but somthing went wrong <br>" + " [error code = " + e + "]";

			}

		});

		// Just for fun
		get("/", (req, res) -> {
			res.type("text/html");
			return Pages.getSkySpy()
					+ "<br><br><br><br><br><br><br>We hope to see you when it's done.";
		});

		// Just for fun
		get("/skyspy", (req, res) -> Pages.getSkySpy());

		// Just for fun
		notFound((req, res) -> {
			res.type("text/html");
			return Pages.get404();
		});

		internalServerError("<html><body><h1>Custom 500 handling</h1></body></html>");

	
	}

}