package tk.skyspy.util;

import tk.skyspy.model.ZipCode;

public class ZipCodeUtil {

	public static ZipCode getZipCode(String zipNoumber) {
		return ZipCodeParser.parseZipCode(zipNoumber);
	}

	public static boolean isValidZipCode(String zipNoumber) {

		if (zipNoumber.length() != 5)
			return false;

		for (int i = 0; i < zipNoumber.length(); i++) {

			try {
				Integer.parseInt(zipNoumber);
			} catch (NumberFormatException e) {
				return false;
			}
		}
		if (getZipCode(zipNoumber).getZip().equals(""))
			return false;
		else
			return true;
	}

}
