package tk.skyspy.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONException;
import org.json.JSONObject;

import tk.skyspy.model.ZipCode;

public class ZipCodeParser {

	public static JSONObject zipJson = makeJson();

	private static JSONObject makeJson() {
		try {
			return new JSONObject(Files.readString(Paths
					.get(Paths.get("").toAbsolutePath().toString(), "Resources/zipcode.json")));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ZipCode parseZipCode(String zipNoumber) {

		if (!(zipJson.optJSONObject(zipNoumber) == null)) {
			JSONObject root = zipJson.optJSONObject(zipNoumber);
			String zip = zipNoumber;
			String city = root.optString("city");
			String state = root.optString("state");
			Double latitude = root.optDouble("latitude");
			Double longitude = root.optDouble("longitude");

			return new ZipCode(zip, city, state, latitude, longitude);
		}

		return new ZipCode();

	}
}
