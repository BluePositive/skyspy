package tk.skyspy.util;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import tk.skyspy.sms.Messages;

public class APIRequester {
	private String beginUrl = "https://api.forecast.io/forecast/";
	private String apiKey;
	private Double latitude;
	private Double longitude;
	private String response = "";
	private String endUrl;

	public APIRequester(String apiKey, Double latitude, Double longitude) {
		this.apiKey = apiKey;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public void request() {
		makeRequest(buildRequeste());
	}

	public String getResponse() {
		return response;
	}

	public String getUrl() {
		return endUrl;
	}

	public String buildRequeste() {
		return beginUrl + apiKey + "/" + latitude + "," + longitude + "?";
	}

	public String makeRequest(String requestString) {
		HttpClient httpClient = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder(URI.create(requestString)).build();
	
		HttpResponse<String> response = null;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e) {
			return Messages.getException(e);
		} catch (InterruptedException e) {
			return Messages.getException(e);
		}
		this.response = response.body();
		return response.body();
	}

}


