package tk.skyspy.sms;

import tk.skyspy.model.ZipCode;
import tk.skyspy.sms.util.Parser;
import tk.skyspy.util.APIRequester;
import tk.skyspy.util.ZipCodeUtil;

public class SmsProcessor {

	public static String process(String smsRequest) {
		String zipNoumber = "";
		String key = "";
		smsRequest = smsRequest.trim().toUpperCase();

		if (smsRequest.startsWith("KEY"))
			return Messages.getKEY();

		if (smsRequest.startsWith("SUPPORT"))
			return Messages.getSUPPORT();

		if (smsRequest.startsWith("ABOUT"))
			return Messages.getABOUT();

		try {

			if (smsRequest.length() >= 6) {
				zipNoumber = smsRequest.substring(0, 6).trim();
				if (smsRequest.length() >= 7)
					key = smsRequest.substring(6).trim();
			} else
				zipNoumber = smsRequest;

			if (!ZipCodeUtil.isValidZipCode(zipNoumber))
				return zipNoumber
						+ " is not a valid ZipCode please send a valid ZipCode\n\nSend SUPPORT for support.";

			ZipCode zipCode = ZipCodeUtil.getZipCode(zipNoumber);

			Parser.setRegion(zipCode);

			//This is my key if you want to use it but please get yourself a key it's free!
			APIRequester requester = new APIRequester("10900d3d72b5106cdffb5fc0ace732e6",
					zipCode.getLatitude(), zipCode.getLongitude());
			requester.request();

			if (!requester.getResponse().isEmpty()) {
				if (key.isEmpty())
					return Parser.parse(requester.getResponse());
				// else
				return Parser.parse(requester.getResponse(), key);
			}
			// else
			return Messages.getResponseNull();

		} catch (NullPointerException e) {
			return Messages.getNullPointerException(e);
		} catch (Exception e) {
			return Messages.getException(e);
		}

	}
}
