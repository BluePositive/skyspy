package tk.skyspy.sms;

public class Messages {

	public static String getKEY() {
		return "SkySpySms KEYWORDS \n\n" + "TODAY = the weather for current day.\n"
				+ "T = the weather for the current day.\n" + "CURRENT = the current weather.\n"
				+ "C = the current weather.\n" + "HOURLY = the weather for the next 12 hours.\n"
				+ "H = the weather for the next 12 hours.\n"
				+ "WEEKLY = the weather for the next 7 days.\n"
				+ "W = the weather for the next 7 days.\n"
				+ "\nSend 5 digit ZIP code with the key word.\n"
				+ "If you don't send a key word it will give the weather for TODAY.\n"
				+ "The key word can be upper case or lower case.\n\n" + "Send SUPPORT for support.";
	}

	public static String getSUPPORT() {
		return "SkySpySms SUPPORT \n\n"
				+ "Send 5 digit ZIP code to get the default weather.\nSend 5 digit ZIP code + key word"
				+ " to get the weather for a particular time frame (12733 HOURLY = weather for the next 12 hours).\n"
				+ "Send  KEY for all available key words.\n"
				+ "For more support send a email to SkySpyUS@gmail.com";
	}

	public static String getABOUT() {
		return "SkySpySms ABOUT \n\n SkySpySms is a part of the SkySpy project WWW.SkySpy.tk\n\n"
				+ "Send SUPPORT for support.";
	}

	public static String getResponseNull() {
		return "Sorry but somthing wend wrong I'm not sure what. "
				+ "\nIt sounds like the DarkSky API is down";
	}

	public static String getNullPointerException(NullPointerException e) {
		return "Sorry but somthing wend wrong I'm not sure what. "
				+ "\nMake sure you entered a valid key word, \n"
				+ "and maybe this can help you\n[error code = " + e.getMessage() + "]";
	}

	public static String getException(Exception e) {
		return "Sorry but somthing wend wrong I'm not sure what. "
				+ "\nMaybe this can help you \n [error code = " + e.getMessage() + "]";
	}
}
