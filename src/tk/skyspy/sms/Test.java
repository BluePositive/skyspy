package tk.skyspy.sms;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import tk.skyspy.model.ZipCode;
import tk.skyspy.sms.util.Parser;
import tk.skyspy.util.APIRequester;
import tk.skyspy.util.ZipCodeUtil;

public class Test {

	public static String process(String testRequest) {
		String zipNoumber = "";
		String key = "";
		testRequest = testRequest.trim().toUpperCase();

		if (testRequest.startsWith("KEY"))
			return Messages.getKEY();

		if (testRequest.startsWith("SUPPORT"))
			return Messages.getSUPPORT();

		if (testRequest.startsWith("ABOUT"))
			return Messages.getABOUT();

		try {

			if (testRequest.length() >= 6) {
				zipNoumber = testRequest.substring(0, 6).trim();
				if (testRequest.length() >= 7)
					key = testRequest.substring(6).trim();
			} else
				zipNoumber = testRequest;

			if (!ZipCodeUtil.isValidZipCode(zipNoumber))
				return zipNoumber
						+ " is not a valid ZipCode please send a valid ZipCode\n\nSend SUPPORT for support.";

			ZipCode zipCode = ZipCodeUtil.getZipCode(zipNoumber);

			Parser.setRegion(zipCode);
			APIRequester requester = new APIRequester("10900d3d72b5106cdffb5fc0ace732e6",
					zipCode.getLatitude(), zipCode.getLongitude());
			requester.request();

			if (!requester.getResponse().isEmpty()) {
				if (key.isEmpty())
					return Parser.parse(requester.getResponse());
				// else
				return Parser.parse(requester.getResponse(), key);
				// If your'e offline it execute this else block (it will use a dummy json file)
			} else {
				if (key.isEmpty())
					return Parser.parse(getTestJson())
							+ "\nThis is just a dummy file for testing (only the State and City is true)";
				// else
				return Parser.parse(getTestJson(), key)
						+ "\nThis is just a dummy file for testing (only the State and City is true)";
			}

		} catch (NullPointerException e) {
			return Messages.getNullPointerException(e);
		} catch (Exception e) {
			return Messages.getException(e);
		}

	}

	public static String getTestJson() {
		String testJson = "";
		try {
			testJson = Files.readAllLines(
					Paths.get(Paths.get("").toAbsolutePath().toString(), "Resources/test.json"))
					.get(0).toString();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return testJson;
	}

}
