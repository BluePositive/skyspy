package tk.skyspy.sms.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tk.skyspy.model.ZipCode;

public class Parser {

	private static JSONObject currently = null;
	private static JSONObject hourly = null;
	private static JSONArray hourlyArry = null;
	private static JSONObject daily = null;
	private static JSONArray dailyArry = null;
	private static String city = "";
	private static String state = "";

	public static void setJsonVariables(JSONObject json) {
		currently = json.optJSONObject("currently");
		hourly = json.optJSONObject("hourly");
		hourlyArry = json.optJSONObject("hourly").optJSONArray("data");
		daily = json.optJSONObject("daily");
		dailyArry = json.optJSONObject("daily").optJSONArray("data");
		ParserUtil.setJsonVariables(json);
	}

	public static void setRegion(ZipCode zipCode) {
		city = zipCode.getCity();
		state = zipCode.getState();

	}

	public static JSONObject makeJson(String jsonString) {

		try {
			return new JSONObject(jsonString);
		} catch (JSONException e) {
			return null;
		}
	}

	public static String parse(String responseString) {
		setJsonVariables(makeJson(responseString));

		return parseToday();
	}

	public static String parse(String responseString, String key) {
		setJsonVariables(makeJson(responseString));

		if ("T".equals(key) || "TODAY".equals(key))
			return parseToday();
		if ("W".equals(key) || "WEEKLY".equals(key))
			return parseWeekly();
		if ("H".equals(key) || "HOURLY".equals(key))
			return parseHourly();
		if ("C".equals(key) || "CURRENT".equals(key))
			return parseCurrent();

		return parseToday();
	}

	public static String parseToday() {
		StringBuilder massge = new StringBuilder();
		massge.append(city + " " + state + "\n" + ParserUtil.parseEpochD(currently.optLong("time")))
				.append("\n\n"
						+ ParserUtil.parseSummary(dailyArry.optJSONObject(0).optString("summary"))
						+ "\n");
		double precipProbability = dailyArry.optJSONObject(0).optDouble("precipProbability");
		if (precipProbability > 0.0)
			massge.append(ParserUtil.parsePercentage(precipProbability) + " chance of "
					+ dailyArry.optJSONObject(0).optString("precipType") + ".\n");
		massge.append("Highest temperature " + ParserUtil
				.parseTemperature(dailyArry.optJSONObject(0).optDouble("temperatureHigh")))
				.append("�F. \nLowest temperature " + ParserUtil
						.parseTemperature(dailyArry.optJSONObject(0).optDouble("temperatureLow")))
				.append("�F.\nHumidity " + ParserUtil
						.parsePercentage(dailyArry.optJSONObject(0).optDouble("humidity")))
				.append(".\nCloudy " + ParserUtil
						.parsePercentage(dailyArry.optJSONObject(0).optDouble("cloudCover")))
				.append(".\nWind speed " + dailyArry.optJSONObject(0).optDouble("windSpeed")
						+ " MPH, ")
				.append("Wind gust " + dailyArry.optJSONObject(0).optDouble("windGust")
						+ " MPH.\n\n" + ParserUtil.getAlertMassge()
						+ "\nSend SUPPORT for support.");

		return massge.toString().trim();
	}

	public static String parseWeekly() {
		StringBuilder massge = new StringBuilder();
		massge.append(city + " " + state + "\n")
				.append(ParserUtil.parseEpochD(currently.optLong("time")) + "\n")
				.append("\n" + ParserUtil.parseSummary(daily.optString("summary")) + "\n\n");
		for (int i = 0; i < 7; i++) {
			massge.append(ParserUtil.parseEpochE(dailyArry.optJSONObject(i).optLong("time")) + "\n")
					.append(ParserUtil.parseSummary(dailyArry.optJSONObject(i).optString("summary"))
							+ "\n");
			double precipProbability = dailyArry.optJSONObject(i).optDouble("precipProbability");
			if (precipProbability > 0.0)
				massge.append(ParserUtil.parsePercentage(precipProbability) + " chance of "
						+ dailyArry.optJSONObject(i).optString("precipType") + ".\n");
			massge.append("Highest temperature " + ParserUtil
					.parseTemperature(dailyArry.optJSONObject(i).optDouble("temperatureHigh")))
					.append("�F.\nLowest temperature " + ParserUtil.parseTemperature(
							dailyArry.optJSONObject(i).optDouble("temperatureLow")))
					.append("�F.\n\n");
		}
		massge.append(ParserUtil.getAlertMassge() + "\nSend SUPPORT for support.");
		return massge.toString().trim();

	}

	public static String parseHourly() {
		StringBuilder massge = new StringBuilder();
		massge.append(city + " " + state + "\n")
				.append(ParserUtil.parseEpoch(currently.optLong("time")) + "\n")
				.append("\n" + ParserUtil.parseSummary(hourly.optString("summary")) + "\n\n");
		for (int i = 0; i <= 12; i++) {
			massge.append(
					ParserUtil.parseEpochH(hourlyArry.optJSONObject(i).optLong("time")) + "\n")
					.append(hourlyArry.optJSONObject(i).optString("summary") + "\n");
			double precipProbability = hourlyArry.optJSONObject(i).optDouble("precipProbability");
			if (precipProbability > 0.0)
				massge.append(ParserUtil.parsePercentage(precipProbability) + " chance of "
						+ hourlyArry.optJSONObject(i).optString("precipType") + ".\n");
			massge.append("Temperature ")
					.append(ParserUtil
							.parseTemperature(hourlyArry.optJSONObject(i).optDouble("temperature")))
					.append("�F.\n\n");
		}
		massge.append(ParserUtil.getAlertMassge() + "\nSend SUPPORT for support.");
		return massge.toString().trim();
	}

	public static String parseCurrent() {
		StringBuilder massge = new StringBuilder();
		massge.append(city + " " + state + "\n")
				.append(ParserUtil.parseEpoch(currently.optLong("time")) + "\n\n")
				.append(ParserUtil.parseSummary(currently.optString("summary")) + ".\n");
		double precipProbability = currently.optDouble("precipProbability");
		if (precipProbability > 0.0)
			massge.append(ParserUtil.parsePercentage(precipProbability) + " chance of "
					+ currently.opt("precipType") + ".\n");
		massge.append(
				"Temperature is " + ParserUtil.parseTemperature(currently.optDouble("temperature")))
				.append("�F feels like ")
				.append(ParserUtil.parseTemperature(currently.optDouble("apparentTemperature"))
						+ "�F.\n")
				.append("Humidity " + ParserUtil.parsePercentage(currently.optDouble("humidity"))
						+ ".\n")
				.append("Cloudy " + ParserUtil.parsePercentage(currently.optDouble("cloudCover")))
				.append("\nWind speed " + currently.optDouble("windSpeed") + " MPH, ")
				.append("Wind gust " + currently.optDouble("windGust") + " MPH.\n\n")
				.append(ParserUtil.getAlertMassge() + "\nSend SUPPORT for support.");
		return massge.toString().trim();
	}
}
