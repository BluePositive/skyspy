package tk.skyspy.sms.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

public class ParserUtil {

	private static String timeZ = null;
	private static String alertMassge = null;

	public static void setTimeZ(JSONObject json) {
		timeZ = json.optString("timezone");
	}

	public static String getTimeZ() {
		return timeZ;
	}

	public static void setAlertMassge(JSONObject json) {
		StringBuilder massge = new StringBuilder();
		JSONArray alerts = json.optJSONArray("alerts");
		if (alerts != null) {
			for (int i = 0; i < alerts.length(); i++) {
				massge.append("/t/tALERT/n" + alerts.optJSONObject(i).optString("title") + "./n")
						.append(alerts.optJSONObject(i).optString("description"))
						.append("/n For more info go to ")
						.append(alerts.optJSONObject(i).optString("uri"));
			}
			alertMassge = massge.toString();
		}
		alertMassge = "";
	}

	public static String getAlertMassge() {
		return alertMassge;
	}

	public static void setJsonVariables(JSONObject json) {
		setTimeZ(json);
		setAlertMassge(json);
	}

	public static String parseEpoch(long epoch) {
		LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(epoch * 1000),
				TimeZone.getTimeZone(getTimeZ()).toZoneId());
		return dateTime.format(DateTimeFormatter.ofPattern("EE MMM d yyyy h:mm a", Locale.ENGLISH));
	}

	public static String parseEpochD(long epoch) {
		LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(epoch * 1000),
				TimeZone.getTimeZone(getTimeZ()).toZoneId());
		return dateTime.format(DateTimeFormatter.ofPattern("EE MMM d yyyy", Locale.ENGLISH));
	}

	public static String parseEpochE(long epoch) {
		LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(epoch * 1000),
				TimeZone.getTimeZone(getTimeZ()).toZoneId());
		return dateTime.format(DateTimeFormatter.ofPattern("EEEE", Locale.ENGLISH));
	}

	public static String parseEpochH(long epoch) {
		LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(epoch * 1000),
				TimeZone.getTimeZone(getTimeZ()).toZoneId());
		return dateTime.format(DateTimeFormatter.ofPattern("h:mm a", Locale.ENGLISH));
	}

	public static String parseTemperature(Double temperature) {
		return temperature.toString().substring(0, 2).replace('.', ' ').trim();
	}

	public static String parsePercentage(Double percentage) {
		return (int) (percentage * 100) + "%";
		/*
		 * if (percentage <= 0.25) return "Very low"; else if (percentage <= 0.50)
		 * return "Low"; else if (percentage <= 0.75) return "High"; else //
		 * if(percentage <= 1.00) return "Very high";
		 */
	}

	public static String parseSummary(String summary) {
		return summary.replaceAll("�", "");
	}

}
